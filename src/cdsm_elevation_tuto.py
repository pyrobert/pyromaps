"""
    Read CSV file with elevation data from GeoGratis (ouvert.canada.ca) and plots the relief
"""

import csv
import numpy as np
from PIL import Image

from pyromaps import pyrocolors

#Elevation data file
CDSM_FILENAME = '../data/CDSM/FME_5644441C_1510799609134_3404/pointData.csv'
COLORMAP_FILENAME = 'elevation_colors.conf'
LIGHT_INFO = {'azimut': 300, 'zenith': 45}

def elevation_to_image(elevation, colormap_filename, light_info):
    """
        Converts a 2D array to RGB image
    """
    #The color is displayed according to elevation using interpolation based on this file
    elevation_colors = []
    with open(colormap_filename, "rb") as colormap_file:
        for i in colormap_file.readlines():
            tmp = i.split()
            elevation_colors.append((float(tmp[0]), float(tmp[1]), float(tmp[2]), float(tmp[3])))

    # Gradient computation
    elevation_gradient = np.gradient(elevation)

    map_img = Image.new('RGB', np.shape(elevation))
    pixels = map_img.load()

    ## Gradient XY > Azimut Axis rotation

    # Azimut in degrees is clockwise from North, theta is counter-clockwise from East
    rotation_theta = 90-light_info["azimut"]
    azimut_sin = np.sin(rotation_theta)
    azimut_cos = np.cos(rotation_theta)

    elevation_gradient_azimut = np.zeros(map_img.size)
    for i in range(map_img.size[0]):    # for every pixel:
        for j in range(map_img.size[1]):
            elevation_gradient_azimut[i, j] = elevation_gradient[0][i, j] * azimut_cos + elevation_gradient[1][i, j] * azimut_sin

    gradient_scale = np.max(np.abs(elevation_gradient_azimut))*1.200

    for i in range(map_img.size[0]):    # for every pixel:
        for j in range(map_img.size[1]):
            # set the colour according to elevation, normalized
            pixels[i, j] = pyrocolors.pixelcolor(elevation[i, j], elevation_colors, elevation_gradient_azimut[i, j], gradient_scale, 0)
    return map_img

def cdsm_file_to_elevation(cdsm_filename):
    """
        Converts a CSDM elevation file into a 2D array
    """
    with open(cdsm_filename, 'r') as cdsm_file:
        elevation_points_reader = csv.reader(cdsm_file, delimiter=',', quotechar='|')
        next(elevation_points_reader)

        elevation_points = np.array(list(elevation_points_reader)).astype("float")

        nb_points_x = 1
        point_x = elevation_points[0, 1]

    #Counting the number of points on the X axis
    while(elevation_points[nb_points_x, 1] == point_x):
        nb_points_x += 1

    #The number of points on the X axis is implicit
    nb_points_y = int(len(elevation_points)/nb_points_x)

    elevation = np.transpose(elevation_points[:, 2].reshape(nb_points_y, nb_points_x))

    return elevation

if __name__ == '__main__':

    ELEVATION = cdsm_file_to_elevation(CDSM_FILENAME)
    IMG = elevation_to_image(ELEVATION, COLORMAP_FILENAME, LIGHT_INFO)

    IMG.save('MontSaintHilaire.bmp')
    IMG.show()

"""
    PYROCOLORS: give colors to your pyromap.
"""
import  numpy as np

def pixelcolor(elevation, elevation_colormap, elevation_gradient_azimut, gradient_scale, light_settings):
    """
        This pixelcolor function gives the RGB color of a pixel according to the elevation,
        the slope gradient and the direction of the slope
    """
    #elevation_max = elevation_colormap
    #elevation_scale = 255/elevation_max
    #find elevation layer from color map
    this_colormap_idx = 1

    for colormap_idx in range(len(elevation_colormap)):
        if elevation < elevation_colormap[colormap_idx][0]:
            this_colormap_idx = colormap_idx
            break

    colormap_elevation_base = elevation_colormap[this_colormap_idx-1][0]
    colormap_idx_drop = elevation_colormap[this_colormap_idx][0]-colormap_elevation_base
    elevation_colormap_ratio = (elevation-colormap_elevation_base)/colormap_idx_drop

    # set the colour according to elevation, normalized
    colormap_red_base = elevation_colormap[this_colormap_idx-1][1]
    colormap_red_drop = elevation_colormap[this_colormap_idx][1]-colormap_red_base
    colormap_green_base = elevation_colormap[this_colormap_idx-1][2]
    colormap_green_drop = elevation_colormap[this_colormap_idx][2]-colormap_green_base
    colormap_blue_base = elevation_colormap[this_colormap_idx-1][3]
    colormap_blue_drop = elevation_colormap[this_colormap_idx][3]-colormap_blue_base

    #Hue depends on elevation
    this_pixel_red = np.uint8(colormap_red_base+colormap_red_drop*elevation_colormap_ratio)
    this_pixel_green = np.uint8(colormap_green_base+colormap_green_drop*elevation_colormap_ratio)
    this_pixel_blue = np.uint8(colormap_blue_base+colormap_blue_drop*elevation_colormap_ratio)

    # Brightness depends on gradient
    elevation_gradient_scaled = elevation_gradient_azimut/gradient_scale
    if np.abs(elevation_gradient_azimut) == 0:
        this_pixel_red = np.uint8(160)
        this_pixel_green = np.uint8(220)
        this_pixel_blue = np.uint8(235)
    elif elevation_gradient_azimut > 0:
        this_pixel_red = np.uint8((1-elevation_gradient_scaled)*this_pixel_red)
        this_pixel_green = np.uint8((1-elevation_gradient_scaled)*this_pixel_green)
        this_pixel_blue = np.uint8((1-elevation_gradient_scaled)*this_pixel_blue)
    else:
        this_pixel_red = np.uint8(min(255, this_pixel_red-elevation_gradient_scaled*(255-this_pixel_red)))
        this_pixel_green = np.uint8(min(255, this_pixel_green-elevation_gradient_scaled*(255-this_pixel_green)))
        this_pixel_blue = np.uint8(min(255, this_pixel_blue-elevation_gradient_scaled*(255-this_pixel_blue)))

    this_pixel_color = (this_pixel_red, this_pixel_green, this_pixel_blue)
    return this_pixel_color
